

# Installing FlectraHQ 3.0 ALFA Docker Compose. For production. 

## Functionality: FlectraHQ 3 + SSL(Caddy Server) + Dozzle + NetData

1. Ubuntu 22.04 operating system
2. Quick installation and configuration of Docker Composer (setup.sh )
3. Saving data in Volumes
4. SSL certificate (auto-renewal) Let's Encrypt CaddyServer (Caddyfile)
5. Password protection of the database page (Caddyfile)
6. Monitoring logs - Dazzle 
7. Server monitoring - NetData
8. Creating a database user for connecting BI (user-read-only.sh )
9. Installing packages of additional Python packages (entrypoint.sh )
10. Updating the FlectraHQ 3 container (under development)
11. Remote data backup (under development)


## Quick Installation

Before installation, you need to create one domain and two subdomains and send them to DNS on IP servers:
1. `flectra-domain.com` (domain)
2. `dozzle.flectra-domain.com`(subdomain)
3. `netdata.flectra-domain.com`(subdomain)


Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/flectra-3-prod-docker-compose/-/raw/master/setup.sh | sudo bash -s
```

Download Flectra 3.0 instance:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/flectra-3-prod-docker-compose/-/raw/master/download.sh | sudo bash -s flectra3.0
```


If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd flectra3.0
```


# SSL - HTTPS Caddy Server

To change the domains in the `Caddyfile` to your own
Specify one domain and two subdomains for applications in the settings configuration `Caddyfile`

``` bash
# Flectra
https://flectra-domain.com:443 {
    reverse_proxy :7073
    encode zstd gzip
# 	tls admin@example.org
    file_server
...
}

# Dozzle
https://dozzle.domain.com:443 {
    reverse_proxy :8888
    encode zstd gzip
# 	tls admin@example.org	
    file_server
...
}


# NetData
https://netdata.flectra.com:443 {
    reverse_proxy :19999
#	tls admin@example.org
	encode zstd gzip
    file_server
}
```
# Specify the domain in the Net Data Docker Compose

Specify the domain

``` bash
    hostname: flectra-domain.com # set to fqdn of host
```


# Monitor Docker logs Dozzle

Dozzle is a small lightweight application with a web based interface to monitor Docker logs. It doesn’t store any log files. It is for live monitoring of your container logs only.


Dozzle will be available at `https://dozzle.domain.com`.


To log in using a username and password, use [instructions](https://dozzle.dev/guide/authentication):


Edit the file ``./auth/data/users.yml`` according to [instructions](https://dozzle.dev/guide/authentication#file-based-user-management) 




# Run Flectra:

``` bash
docker-compose up -d
```


# Create a password for the Flectra database page

! Run this command after startup `docker-compose up -d`

The page `/web/database/` password protected

``` bash
docker exec -it caddy caddy hash-password
```

Enter the password and get it in the console
``` bash
docker exec -it caddy caddy hash-password
Enter password:
Confirm password:
hash-password
```

Uncomment the lines in `Caddyfile``

``` bash
    basicauth /web/database/* {
	flectra hash-password   # password: hash-password, Login: flectra
    } 
```

# Database management
``` bash
/web/database/manager
```

``` bash
/web/database/selector
```

# Create a password for the NetData


The page `https://netdata.flectra.con` password protected

``` bash
docker exec -it caddy caddy hash-password
```

Enter the password and get it in the console
``` bash
docker exec -it caddy caddy hash-password
Enter password:
Confirm password:
hash-password
```

Uncomment the lines in `Caddyfile`

``` bash
	basicauth / {
	netdata hash-password   # password: hash-password, Login: netdata
    }  
```

# Create a user (postgresql) for BI access

``` bash
curl -OL https://gitlab.com/6ministers/business-apps/flectra-3-dev-docker-compose/-/raw/master/user-read-only.sh
```

Set Variables

``` bash
# Define variables
USER=read_only
PASS=password
DATABASE=test
```

Make settings in the configuration `postgresql`

`pg_hba.conf`
``` bash
# IPv4 remote connection
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    database        flectra         IP/32                   md5
```
`postgresql.conf`
``` bash
listen_addresses = '*'
					# comma-separated list of addresses;
					# defaults to 'localhost'; use '*' for all
					# (change requires restart)
port = 5432				# (change requires restart)
max_connections = 100			# (change requires restart)
```

Activate the script
``` bash
chmod +x user-read-only.sh
```

``` bash
sudo ./user-read-only.sh
```


## Usage

- **If you get any permission issues**, change the folder permission to make sure that the container is able to access the directory:

``` sh
$ sudo chmod -R 777 addons
$ sudo chmod -R 777 etc
$ sudo chmod -R 777 postgresql
```


- To run Flectra container in detached mode (be able to close terminal without stopping Flectra):

```
docker-compose up -d
```

- To Use a restart policy, i.e. configure the restart policy for a container, change the value related to **restart** key in **docker-compose.yml** file to one of the following:
   - `no` =	Do not automatically restart the container. (the default)
   - `on-failure[:max-retries]` =	Restart the container if it exits due to an error, which manifests as a non-zero exit code. Optionally, limit the number of times the Docker daemon attempts to restart the container using the :max-retries option.
  - `always` =	Always restart the container if it stops. If it is manually stopped, it is restarted only when Docker daemon restarts or the container itself is manually restarted. (See the second bullet listed in restart policy details)
  - `unless-stopped`	= Similar to always, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts.
```
 restart: always             # run as a service
```

## Custom addons

The **addons/** folder contains custom addons. Just put your custom addons if you have any.

 

## Flectra container management

**Run Flectra**:

``` bash
docker-compose up -d
```

**Restart Flectra**:

``` bash
docker-compose restart
```

**Stop Flectra**:

``` bash
docker-compose down
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

Go to the catalog
``` bash
cd flectra3.0
```

Restart
``` bash
docker-compose restart dozzle
```

``` bash
docker-compose restart caddy
```

``` bash
docker-compose restart netdata
```

``` bash
docker-compose restart flectra3.0
```

``` bash
docker-compose restart phpldapadmin
```

``` bash
docker-compose restart openldap
```

Flectra Update

``` bash
cd flectra3.0
```
``` bash
sudo docker-compose down
```

``` bash
docker pull flectrahq/flectra:3.0
```
``` bash
sudo docker-compose up -d
```

## Live chat



## docker-compose.yml

* flectrahq/flectra:3.0
* postgres:16


# Documentation

https://dozzle.dev/guide/authentication
